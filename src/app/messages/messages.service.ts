import { Injectable } from '@angular/core';
import {Http , Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
@Injectable()
export class MessagesService {
  http:Http;
getMessages(){
 //return ['Message1','Message2', 'Message3','Message4'];
 //get messages from the SLIM REST API 
  return this.http.get('http://localhost/angular/slim/messages');
}
postMessage(data){
  let options = {
    headers:new Headers({
      'content-type':'application/x-www-form-urlencoded'
    })
  }
  let params = new HttpParams().append('message', data.message);
  return this.http.post('http://localhost/angular/slim/messages',
params.toString(), options);

}
deleteMessage(key){
  return this.http.delete('http://localhost/angular/slim/messages/'+ key);
}

  constructor(http:Http) { 
    this.http = http;
  }
    
}
